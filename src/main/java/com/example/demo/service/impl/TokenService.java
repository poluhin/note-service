package com.example.demo.service.impl;

import com.example.demo.db.entity.UserCredentials;
import com.example.demo.db.repository.UserCredRepository;
import com.example.demo.model.Token;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TokenService {

    private static final ObjectMapper MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private static final int DEFAULT_EXPIRATION_TIME_S = 30;

    UserCredRepository userCredRepository;

    public Token getToken(int userId, String password) {
        val expirationTime = Instant.now().getEpochSecond() + DEFAULT_EXPIRATION_TIME_S;
        return new Token(userId, getTokenStr(userId, password), expirationTime);
    }

    public UserCredentials getToken(String strToken) {
        val json = Base64.decodeBase64(strToken.getBytes(StandardCharsets.UTF_8));
        try {
            return MAPPER.readValue(json, UserCredentials.class);
        } catch (JsonProcessingException e) {
            log.error("Cannot process string token {}", strToken, e);
        } catch (IOException e) {
            log.error("Error when reading json", e);
        }
        return null;
    }

    private String getTokenStr(int userId, String password) {
        val cred = userCredRepository.findById(userId)
                .orElse(null);
        if (cred == null || !cred.getPassword().equals(password)) {
            return null;
        }
        try {
            val strCred = MAPPER.writeValueAsString(cred);
            return Base64.encodeBase64String(strCred.getBytes(StandardCharsets.UTF_8));
        } catch (JsonProcessingException e) {
            log.error("Cannot process obj to json for user id {} with pass {} ", userId, password, e);
        }
        return null;
    }

}

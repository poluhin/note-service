package com.example.demo.filter;

import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public abstract class AbstractFilter implements Filter {

    public abstract boolean shouldFilter(HttpRequestExpanded request);

    public abstract boolean filter(HttpRequestExpanded request, HttpServletResponse response);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        val httpReq = new HttpRequestExpanded(request);
        val httpRes = (HttpServletResponse) response;
        if (shouldFilter(httpReq)) {
            if (!filter(httpReq, httpRes)) {
                return;
            }
        }
        chain.doFilter(httpReq, httpRes);
    }
}

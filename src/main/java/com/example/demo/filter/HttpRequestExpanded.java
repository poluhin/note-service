package com.example.demo.filter;

import lombok.val;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static java.lang.Integer.parseInt;
import static java.util.Collections.enumeration;

public class HttpRequestExpanded extends HttpServletRequestWrapper {

    private final Map<String, String> expandedHeaders = new HashMap<>();

    public HttpRequestExpanded(ServletRequest request) {
        super((HttpServletRequest) request);
    }

    public void addHeader(String name, String value) {
        expandedHeaders.put(name, value);
    }

    @Override
    public String getHeader(String name) {
        if (expandedHeaders.containsKey(name)) {
            return expandedHeaders.get(name);
        }
        return super.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        val result = new LinkedList<String>();
        if (expandedHeaders.containsKey(name)) {
            result.add(expandedHeaders.get(name));
        }
        val enumerations = super.getHeaders(name);
        while (enumerations.hasMoreElements()) {
            result.add(enumerations.nextElement());
        }
        return enumeration(result);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        val result = new LinkedList<String>();
        val enumerations = super.getHeaderNames();
        while (enumerations.hasMoreElements()) {
            result.add(enumerations.nextElement());
        }
        result.addAll(expandedHeaders.keySet());
        return enumeration(result);
    }

    @Override
    public int getIntHeader(String name) {
        if (expandedHeaders.containsKey(name)) {
            return parseInt(expandedHeaders.get(name));
        }
        return super.getIntHeader(name);
    }
}
package com.example.demo.model;

import java.io.Serializable;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDTO implements Serializable {

    int id;
    String name;
    String avatarLink;
    int countOfNotes;
    String password;

}

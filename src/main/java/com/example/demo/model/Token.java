package com.example.demo.model;

import lombok.Value;

@Value
public class Token {

    Integer userId;
    String token;
    long expiration;

}

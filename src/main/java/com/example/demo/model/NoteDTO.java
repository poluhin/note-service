package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NoteDTO implements Serializable {

    Integer id;
    String text;
    UserDTO author;
    LocalDateTime createdDateTime;
    LocalDateTime editedDateTime;

}

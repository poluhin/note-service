package com.example.demo.db.repository;

import com.example.demo.db.entity.UserCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCredRepository extends JpaRepository<UserCredentials, Integer> {
}

package com.example.demo.db.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "user_cred")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserCredentials {

    @Id
    @Column(name = "user_id")
    int userId;

    String password;

}

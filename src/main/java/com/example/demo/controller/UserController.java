package com.example.demo.controller;

import static java.util.Optional.ofNullable;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

import com.example.demo.model.UserDTO;
import com.example.demo.service.impl.UserService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserController {

    UserService service;

    @PostMapping("/")
    public ResponseEntity<UserDTO> createNewUser(@RequestBody UserDTO dto) {
        return ok(service.newUser(dto));
    }

    @PutMapping("/")
    public ResponseEntity<UserDTO> editUser(@RequestHeader(name = "UserId") int id,
                                            @RequestBody UserDTO edited) {
        return ofNullable(service.edit(id, edited))
            .map(ResponseEntity::ok)
            .orElse(notFound().build());
    }

}

package com.example.demo.controller;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

import com.example.demo.model.NoteDTO;
import com.example.demo.service.impl.NoteService;
import java.util.Collection;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/note")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class NoteController {

    NoteService noteService;

    @GetMapping("/{id}")
    public ResponseEntity<NoteDTO> getNote(@PathVariable int id) {
        val note = noteService.getNoteById(id);
        if (note == null) {
            return notFound().build();
        }
        return ok(note);
    }

    @PostMapping("/")
    public ResponseEntity<NoteDTO> createNewNote(@RequestHeader("UserId") int author,
                                                 @RequestBody NoteDTO dto) {
        return ok(noteService.newNote(author, dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<NoteDTO> editNote(@RequestHeader("UserId") int author,
                                            @PathVariable int id,
                                            @RequestBody NoteDTO dto) {
        if (!noteService.isAuthor(id, author)) {
            return status(HttpStatus.FORBIDDEN).build();
        }

        val result = noteService.edit(id, author, dto);
        if (result == null) {
            return notFound().build();
        }

        return ok(result);
    }

    @GetMapping("/by/author/{authorId}")
    public ResponseEntity<Collection<NoteDTO>> getNotesByAuthor(@PathVariable int authorId) {
        return ok(noteService.getByAuthorId(authorId));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNote(@RequestHeader("UserId") int author,
                                           @PathVariable int id) {
        if (!noteService.deleteNote(author, id)) {
            return status(HttpStatus.FORBIDDEN).build();
        }

        return ok().build();
    }


}

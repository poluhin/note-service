package com.example.demo.controller;

import com.example.demo.model.Token;
import com.example.demo.model.UserDTO;
import com.example.demo.service.impl.TokenService;
import com.example.demo.service.impl.UserService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthController {

    UserService userService;
    TokenService tokenService;

    @PutMapping("/token")
    public ResponseEntity<Token> generateNewToken(@RequestBody UserDTO user) {
        if (user.getPassword() == null || user.getPassword().isBlank()) {
            return status(HttpStatus.FORBIDDEN).build();
        }

        if (user.getId() != 0) {
            return ok(tokenService.getToken(user.getId(), user.getPassword()));
        }

        val created = userService.newUser(user);
        return ok(tokenService.getToken(created.getId(), user.getPassword()));
    }

}
